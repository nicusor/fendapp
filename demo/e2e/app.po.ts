import { browser, by, element } from 'protractor';

export class MdbTestAngularPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('h4 mb-4')).getText();
  }
}
