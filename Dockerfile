FROM node:10.5

RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -; echo 'deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main' | tee /etc/apt/sources.list.d/google-chrome.list

#Install OS dependencies
RUN apt-get update -qq && apt-get install -y apt-utils build-essential libpq-dev google-chrome-stable

#Export our nodejs env
RUN export NODE_ENV=development

#Update npm
RUN npm install npm@latest -g

# Sets the temp path
ENV NODE_TEMP /usr/temp

# Creates the temp directory and all the parents (if they don’t exist)
RUN mkdir -p $NODE_TEMP

#Use a temporary directory for modules
WORKDIR $NODE_TEMP

# Copy package.json and local node module to container
COPY . .

#Install node modules
RUN npm i 

#RUN cp /usr/temp/demo/src/tsconfig.json /usr/temp/tsconfig.json

#RUN google-chrome-stable -version

#Run test suite
#RUN npm test 

#Expose ports
EXPOSE 4200 49153

#Specify entrypoint
ENTRYPOINT ["npm", "start", "mdb-demo"]
